const fs = require('fs');

module.exports = nombreDeArchivo => {
    const data = fs.readFileSync(nombreDeArchivo).toString();
    const trimmedData = data.trim();
    try {
        const jsonData = JSON.parse(trimmedData);
        if (
            jsonData.iglesias
            && jsonData.iglesias[0]
        ) {
            return jsonData.iglesias.map(
                iglesia => iglesia.nombre
            );
        }
        return jsonData;
    } catch (error) {
        return trimmedData.split('\n');
    }
}