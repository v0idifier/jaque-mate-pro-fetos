# Bienvenidx!
Cuando vimos que unxs anti-derechos decidieron hacer un formulario para reportar a
socorristas abortistas, supimos que teníamos que hacer algo.

Así es como nació JMPF, el proyecto que quiere llenar la base de datos de lxs provida
con información de iglesias.

## Como funciona
Este es el resúmen: nosotras te damos unas "IDs" que identifican iglesias, vos las
pones en nuestro sitio que te va a dar la dirección personalizada junto a una
"identidad" generada con nombre y email falso. Con esa data, llenas su formulario y
repetir!

## Tutorial (?)
Así que querés ayudarnos! gracias <3

1. Contactar a v0idifier en [Telegram](https://telegram.me/v0idifier),
[via Email](mailto:jmpf@riseup.net) o por [el fediverso](https://todon.nl/@v0idifier)
para pedirle un pad<a href="#footnote-1"><sup>[1]</sup></a> con IDs de direcciones (de iglesias). <!--
    The person that created the markdown lib we are using didn't bundle footnotes
    directly, but instead put it as a plugin. The problem? It requires Browserify
    (or some bundling step, which we don't have because it's too much work for a quick
    hack.)
    https://github.com/Kriegslustig/showdown-footnotes
-->

2. Una vez tenés el pad, copiá una ID y pegala en "id de dirección".
<!-- ![una captura de pantalla mostrando donde pegar la ID](/imgs/iddedireccion.png) -->

3. Apretar en "conseguir dirección". Eso te va a devolver la dirección en si, personalizada
con la identidad que se generó cuando cargaste el sitio.

4. **IMPORTANTE**: Fijarse que la identidad o dirección se "vea real". Si la identidad
tiene algo que no parece humano, apretas en "generar nueva identidad". Si la dirección
tiene algo que no parece humano, vas al pad y le ponés una marca, y repetís
el proceso desde paso 2.

5. Copiar "nombre de identidad" a el "nombre del denunciante" del formulario.

6. Copiar "email de identidad" a la "dirección de correo electrónico" y también a "Mail o Teléfono del denunciante" (los cabeza de termo lo pusieron dos veces).

7. Copiar "dirección/localidad (resultado)" a "Localidad y dirección del lugar" de el formulario.

8. Inventarte una descripción en la parte de "Breve descripción del lugar a denunciar". Algo tipo "acá venden misoprostol", "hacen aborto CLANDESTINO". Algo importante a tener en cuenta es que si el email o nombre están escritos CON MAYÚSCULA, probablemente sea una buena idea escribir la descripción de la misma manera.

9. Todo lo demás dejarlo vacio (o inventarse algo creíble) y apretar en enviar!

10. Dormir sabiendo que ayudaste a molestar a lxs provida, o apretar en "generar nueva identidad" y volver al paso 2 hasta que se acaben las IDs.

<small class="footnote" id="footnote-1">
    <a href="#footnote-1">
        <sup>[1]</sup>
    </a>: Un pad es como un "Google Docs". Si tenés el link al pad, podés verlo y editarlo.
</small>