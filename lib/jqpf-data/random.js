let random = {};

random.int = (min, max) =>
    Math.floor(min + Math.random() * max);
random.itemInArray = (items) =>
    items[random.int(0, items.length)];
random.item = (...items) =>
    random.itemInArray(items);

// random.itemFromArray = array => random.item(...array);

// eg: (
//     ['a', 0.6], // 60% for 'a'
//     ['b', 0.4], // 40% for 'b'
// )
random.itemCustom = (...items) => {
    // console.log('aa',items)
    let totalChances = items.reduce(
        (result, current) => result + current[1],
        0,
    );

    if (!(totalChances > .999 && totalChances <= 1)) {
        throw new Error(`Invalid chance! Expected around 1, got ${totalChances}`);
    }

    let lastMax = 0;
    const chanceStuff = items.map(
        ([value, chance]) => ({
            value,
            min: lastMax,
            max: lastMax = (lastMax + chance),
        })
    );

    const random = Math.random();

    for (const { value, min, max } of chanceStuff) {
        // console.log(value)
        if (random > min && random < max) return value;
    }
}

module.exports = random;