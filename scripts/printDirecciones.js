const data = require('../data/conseguirData')('data/direcciones/nominatim/attempt0.json');

const uniq = a => [...new Set(a)];

const getHouseNumber = el => el.house_number;
const getRoad = el => el.road;
const getCity = el => {
    let cities = [];

    if (el.town) cities.push(el.town);
    if (el.neighbourhood) cities.push(el.neighbourhood);
    if (el.city) cities.push(el.city);
    if (el.state) {
        switch (el.state) {
            case 'Cba.': cities.push('Córdoba'); break;
            case 'Sta. Fe': cities.push('Santa Fe'); break;
            case 'Mnes.': cities.push('Mendoza'); break;
            case 'JJY': cities.push('Jujuy'); break;
            case 'Bs. As.': cities.push('Provincia de Buenos Aires'); break;
            // case 'Bs. As.': return 'Buenos Aires';
            default: cities.push(el.state)
        }
    }

    // return cities.join(', ')
    return uniq(cities)
};

let arr = [];

const CITY_WHITELIST = [
    'Córdoba',
    'Santa Fe',
    'Mendoza',
    'Buenos Aires',
    'CABA',
]

data.map(el => {
    // console.log(el)

    const [houseNumber, road, city] = [
        getHouseNumber(el.address),
        getRoad(el.address),
        getCity(el.address),
    ]

    if (!(
        houseNumber
        && road
        && city.length > 0
    )) return;
    
    // if ()

    console.log(`
house_number = ${houseNumber}
road = ${road}
city = ${city.join(', ')}
    `);

    arr.push({ DISPLAYNAME: `${road} ${houseNumber}, ${city.join(', ')}`, houseNumber, road, city, id: el.osm_id });
})

console.log(arr.length)

// require('fs').writeFileSync('./outHuman', arr.map(
//     el => `${el.DISPLAYNAME} || ID=${el.id} || >`
// ).join('\n'));
require('fs').writeFileSync('./outDirecciones.json', JSON.stringify(arr));