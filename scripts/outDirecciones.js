const NOMINATIM_API_HOST = 'https://nominatim.openstreetmap.org';

const input = require('../out.json').elements;
const request = require('request-promise');
const fs = require('fs');

const originalIds = input.map(el => {
    let typeCode;

    switch (el.type) {
        case 'way': typeCode = 'W'; break;
        case 'node': typeCode = 'N'; break;
        case 'relation': typeCode = 'R'; break;
    }

    return `${typeCode}${el.id}`;
});
const ids = [...originalIds];

console.log(`
ids amount = ${originalIds.length}
`)


let array = []

function lookup () {
    const idSlice = ids.splice(0, 50);
    const idsString = idSlice.join(',');

    return request(`${NOMINATIM_API_HOST}/lookup?format=json&osm_ids=${idsString}`)
        .then(res => JSON.parse(res))
        .then(json => {
            // console.log(json);
            // console.log(json.length);
            json.map(el => array.push(el));
            console.log(`amount already = ${array.length}/${originalIds.length}`)
            console.log(`amount already = ${array.length}/${originalIds.length}`)
            if (ids.length === 0) return array;
            else return lookup();
        });
}

for (let index = 0; index < 20; index++) {
    lookup()
        .then(res => {
            console.log(res.length);
            fs.writeFileSync(`attempt${index}.json`, JSON.stringify(res));
        })
        .catch(err => {
            console.error(err);
            process.exit(1);
        });
}

// for (let index = 0; index < ids.length; index += 50) {
//     // console.log(index)
//     const idSlice = ids.slice(index, index + 50);
//     const idsString = idSlice.join(',');

//     // console.log(idsString)

//     request(`https://nominatim.openstreetmap.org/lookup?format=json&osm_ids=${idsString}`)
//         .then(res => JSON.parse(res))
//         .then(json => {
//             // console.log(json);
//             console.log(json.length);
//         })
    
// }

// let out = inp.elements.filter(
//     el => (el.tags
//             && el.tags['addr:housenumber']
//             && el.tags['addr:street']
//             && getCity(el)
//     )
// )
// let outNoCity = inp.elements.filter(
//     el => (el.tags
//             && el.tags['addr:housenumber']
//             && el.tags['addr:street']
//             && !getCity(el)
//     )
// )

// function getCity (el) {
//     return el['addr:city'] || el['is_in:city'];
// }

// out = out.map(el => `${el.tags['addr:street']} ${el.tags['addr:housenumber']}, ${getCity(el)}`)

// console.log(out.join('\n'));
// const ids = outNoCity.map(el => {
//     let typeCode;
// console.log(el.type)
//     switch (el.type) {
//         case 'way': typeCode = 'W'; break;
//         case 'node': typeCode = 'N'; break;
//         case 'relation': typeCode = 'R'; break;
//     }

//     return `${typeCode}${el.id}`;
// });

// for (let index = 0; index < ids.length; index += 50) {
//     // console.log(index)
//     const idSlice = ids.slice(index, index + 50);
//     const idsString = idSlice.join(',');

//     // console.log(idsString)

//     request(`https://nominatim.openstreetmap.org/lookup?format=json&osm_ids=${idsString}`)
//         .then(res => JSON.parse(res))
//         .then(json => {
//             // console.log(json);
//             console.log(json.length);
//         })
    
// }

// // function 

// console.log(out.length);
