const request = require('request-promise');
const fs = require('fs');

const REGEXP = /<div onclick="clickDirTel\(TipoClick\.CLIC_DIR_TEL, (.+)\)" class="m-button--results-business--icon/g;
const TOTAL_REGEXP = /<span class="m-header--count">([0-9]+) resultados<\/span>/;

function promiseNextTick () {
    return new Promise((resolve, reject) => {
        process.nextTick(resolve)
    })
}

function getIglesias (page = 1, out = []) {
//    if (page > 10) return out;
    console.log(`page: ${page}, amount: ${out.length}`)

    if (page % 5 === 0) {
        fs.writeFileSync(`output/${page}.json`, JSON.stringify(out));
        console.log(`saved`)
        out = [];
    }

    return request(`http://www.paginasblancas.com.ar/persona/iglesias/p-${page}`)
        .then(res => {
            // console.log(res);
            // fs.writeFileSync('a', res)
            let match = REGEXP.exec(res);
            while (match != null) {
                out.push(match[1]);
                match = REGEXP.exec(res);
            }
            const total = TOTAL_REGEXP.exec(res)[1];
            if (page >= (total / 15) - 3) {
                return 'done';
            } else {
                return promiseNextTick().then(() => getIglesias(++page, out));
            }
            // let match = res.match(REGEXP)
            // delete match.input;
            // console.log(match);
        });
}

getIglesias().then(res => {
    console.log(res)
});