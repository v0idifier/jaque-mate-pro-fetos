const request = require('request-promise');
const fs = require('fs');

let out = [];
let totalScrapped = 0;
let page = 0;
let total = 0;
let threadsDone = 0;

const THREADS = 5;

const USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0';

const TOTAL_REGEXP = /\|&nbsp;(.+)&nbsp;resultados/;
const CALLTO_REGEXP = /callto:(.+)\d"/gi;

async function check () {
    if (threadsDone) {
        threadsDone++;
        throw 'done';
    }
}

function thread () {
    const url = `https://www.paginasamarillas.com.ar/buscar/q/iglesias/p-${page++ + 1}/`;

    return request(url, {
        headers: {
            'User-Agent': USER_AGENT,
        },
    })
        .then(res => {
            total = TOTAL_REGEXP.exec(res)[1];
            let match = CALLTO_REGEXP.exec(res);
            while (match != null) {
                out.push(match[1]);
                totalScrapped++;
                match = CALLTO_REGEXP.exec(res);
            }

            return check().then(thread);
        })
        .catch(err => {
            if (err.statusCode === 404) {
                threadsDone++;
            } else {
                console.log(err.message)
            }
        });
}

for (let i = 0; i < THREADS; i++) {
    thread()
}

setInterval(() => {
    console.log(`page: ${page}${total?'/'+(Math.floor(total / 25)+1):''} | total scrapped: ${totalScrapped}${total?' | total chruches: '+total:''}`);
    console.log(`threads: ${THREADS} | threads done: ${threadsDone}`);
    if (threadsDone >= THREADS) {
        console.log('finished');
        console.log(`total scrapped: ${totalScrapped}`)
        fs.writeFileSync('output/scrap.json', JSON.stringify(out));
        console.log('saved')
    }
}, 500);