const request = require('request-promise');
const querystring = require('querystring');

if (!process.argv[2]) {
    console.error('por favor poner de primer parametro el nombre del archivo con nombres de iglesias.');
    process.exit(1);
}

const nombres = require('../data/conseguirData')(process.argv[2]);

const hasComma = string => string.indexOf(',') > -1;

function removeLastPart (string) {
    let stringArray = string.split(',');
    if (stringArray.length > 1) {
        stringArray.splice(stringArray.length - 1, 1);
        stringArray = stringArray.map(str => str.trim());
        return stringArray.join(', ');
    } else {
        return string;
    }
}

function loop (index = 0, overrideNombre) {
    if (index >= nombres.length) {
        console.log('listop wache')
        return
    }

    const nombre = overrideNombre || nombres[index];
    const options = {
        q: overrideNombre || nombre + ', Argentina',
        format: 'json',
        // countrycodes: 'ar',
        email: 'v0id@riseup.net',
    };

    // const url = `https://nominatim.openstreetmap.org/search/${nombre}, Argentina?format=json&email=v0id@riseup.net`
    const url = `https://nominatim.openstreetmap.org/search?${querystring.stringify(options)}`;

    // console.log(options.q)

    request(url)
        .then(result => JSON.parse(result))
        .then(results => {
            // console.log(result.length);
            // console.log(result)
            // if (results.length > 0) console.log(results[0].display_name)
            if (results.length > 0) console.log(JSON.stringify(results[0]))
            return results;
        })
        .then(results =>
            results.length == 0 && !overrideNombre
                ? loop(index, nombre)
                : hasComma(nombre)
                    ? loop(index, removeLastPart(nombre))
                    : loop(++index)
        )
}

loop()
