const chalk = require('chalk');

if (!process.argv[2]) {
    console.error('por favor poner de primer parametro el nombre del archivo con direcciones.');
    process.exit(1);
}

const direcciones = require('../data/conseguirData')(process.argv[2]);

const argentinas = direcciones.filter(
    direccion => direccion.indexOf('Argentina') > -1,
);

const noArgentinas = direcciones.filter(
    direccion => direccion.indexOf('Argentina') < 0,
);

console.log(chalk.blue.strikethrough('------------------------------------'));
console.log(                         '############ '
+ chalk.yellow(                                   'argentina')
+                                                          ' #############');
console.log(chalk.blue.strikethrough('------------------------------------'));
for (const direccion of argentinas) {
    console.log(direccion);
}
console.log(chalk.red.strikethrough('------------------------------------'));
console.log(                        '               others               ');
console.log(chalk.red.strikethrough('------------------------------------'));
for (const direccion of noArgentinas) {
    console.log(direccion);
}

