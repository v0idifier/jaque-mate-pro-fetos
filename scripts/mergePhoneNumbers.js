const fs = require('fs');
const path = require('path');

const blancasFilenames = fs.readdirSync(path.join(
    __dirname, '../data/inutil o sources/iglesias-numeros-paginas-blancas'
));
let blancas = [];
for (const filename of blancasFilenames) {
    const file = JSON.parse(fs.readFileSync(path.join(
        __dirname, '../data/inutil o sources/iglesias-numeros-paginas-blancas', filename,
    )));

    blancas = [ ...blancas, ...file ];
}

const amarillas = JSON.parse(fs.readFileSync(path.join(
    __dirname, '../data/inutil o sources/iglesias-numeros-paginas-amarillas/scrap.json',
)));

const total = [ ...new Set([ ...blancas, ...amarillas ]) ];

// console.log(total, total.length)
fs.writeFileSync(
    path.join(__dirname, '../output/merged.json'),
    JSON.stringify(total),
);